module API {

    export interface Lap {
        lap: number;
        ticks: number;
        millis: number;
    }

    export interface LapFinished {
        car: CarId;
        lapTime: Lap;
        raceTime;
        ranking;
    }

    export interface Join {
        name: string;
        key: string;
    }

    export interface CarId {
        name: string;
        color: string;
    }

    export interface CarDimensions {
        length: number;
        width: number;
        guideFlagPosition: number;
    }

    export interface Car {
        id: CarId;
        dimensions: CarDimensions;
    }

    export interface Lane {
        distanceFromCenter: number;
        index: number;
    }

    export interface Piece {
        // straight
        length?: number;
        switch?: boolean;

        // curve
        radius?: number;
        angle?: number;
    }

    export interface Track {
        id;
        name;
        pieces: Piece[];
        lanes: Lane[];
        startingPoint;
    }

    export interface Race {
        track: Track;
        cars: Car;
        raceSession: any;
    }

    export interface GameInit {
        race: Race;
    }

    export interface GameStart {
    }

    export interface GameEnd {
    }

    export interface LanePosition {
        startLaneIndex: number;
        endLaneIndex: number;
    }

    export interface PiecePosition {
        pieceIndex: number;
        inPieceDistance: number;
        lane: LanePosition;
        lap: number;
    }

    export interface CarPosition {
        id: CarId;
        angle: number;
        piecePosition: PiecePosition;
    }

    export interface Turbo {
        turboDurationMilliseconds;
        turboDurationTicks;
        turboFactor;
    }
}

export = API;