var _ = require("../lib/underscore");

module Util {


    export function stringify(obj) {
        return JSON.stringify(obj, null, 4);
    }

    export function round(v: number, d: number) {
        return Math.round(v * Math.pow(10, d)) / Math.pow(10, d);
    }

    export function whiteFrontPad(s: any, w: number) {
        s = s + "";
        while (s.length < w)
            s = " " + s;
        return s;
    }

    export function roundPad(v: number, w: number) {
        var value = round(v, w) + "";
        if (!/\./.test(value)) {
            value += ".";
            while (w-- > 0) {
                value += "0";
            }
        } else {
            while (value.substring(value.indexOf('.') + 1).length < w)
                value += "0";
        }

        return value;
    }

    export function signPrefixRoundPad(v: number, w: number) {
        var value = roundPad(v, w - 1);
        return ((v > 0) ? "+" : "") + value;
    }

    export function debug(obj: any) {
        if (!_.isString(obj))
            obj = stringify(obj);
        console.log('DEBUG: ' + obj);
    }

    export function nvl(value, alternative) {
        return (value) ? value : alternative;
    }

}

export = Util;