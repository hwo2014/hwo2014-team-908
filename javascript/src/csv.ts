var fs = require('fs');
var moment = require('../lib/moment');

module CSV {

    var wstream;

    export function open() {
        wstream = fs.createWriteStream('csv-data-' + moment().format('YYYYMMDD-HHmmss') + '.csv');
    }

    export function write(line) {
        wstream.write(line + '\n');
    }

    export function close() {
        wstream.end();
    }
}

export = CSV;
