
var _ = require("../lib/underscore");

import API = require("./api");
import Util = require("./util");
import CSV = require('./csv');

module Engine {


    class Lane {
        info: API.Lane;

        laneAdjustedRadiusForThis2(radius: number, angle: number): number {
            return radius - sign(angle) * this.info.distanceFromCenter;
        }

        laneAdjustedRadiusForThis(logicalCorner: LogicalCorner): number {
            return this.laneAdjustedRadiusForThis2(logicalCorner.radius, logicalCorner.angle);
        }

        laneAdjustedRadius(piece: Piece): number {
            return this.laneAdjustedRadiusForThis2(piece.info.radius, piece.info.angle);
        }

        laneAdjustedPieceDistance(piece: Piece) {
            if (piece.isCorner()) {
                return Math.abs(piece.info.angle) / 360.0 * 2 * Math.PI * this.laneAdjustedRadius(piece);
            } else {
                return piece.info.length;
            }
        }

        canSwitchLeft() {
            return this.info.index > 0;
        }

        canSwitchRight() {
            return this.info.index < (lanes.length - 1);
        }
    }

    class Piece {
        info: API.Piece;
        index: number;
        next: Piece;
        suitableForTurbo: boolean;
        logicalCorner: LogicalCorner;

        isCorner(): boolean {
            return 'radius' in this.info
        }

        isStraight(): boolean {
            return 'length' in this.info
        }

        consecutiveStraights() {
            var count = 0;
            var p: Piece = this;
            while (p.isStraight()) {
                count++;
                p = p.next;
            }
            return count;
        }

        pieceDistanceApprox() {
            if (this.isCorner()) {
                // lane changes things when in corner, use this only for fyi information
                return Math.abs(this.info.angle) / 360.0 * 2 * Math.PI;
            } else {
                return this.info.length;
            }
        }

        pieceBefore() {
            var before = this;
            while (before.next != this)
                before = before.next;
            return before;
        }
    }

    class Car {
        speed: number = 0;
        acceleration: number = 0;
        angle: number = 0;
        angleSpeed: number = 0;
        angleAcceleration: number = 0;
        throttle: number;
        turboActive: boolean;
    }

    function sign(n: number) {
        return n < 0 ? -1 : 1;
    }

    class Location {

        inPieceDistance: number;
        piece: Piece;
        lane: Lane;

        laneAdjustedRadius(): number {
            return this.lane.laneAdjustedRadius(this.piece);
        }

        laneAdjustedPieceDistance(): number {
            return this.lane.laneAdjustedPieceDistance(this.piece);
        }

        pieceRemainingDistance(): number {
            return this.laneAdjustedPieceDistance() - this.inPieceDistance;
        }

        distanceToNextLogicalCorner(): number {
            var current = this.piece.logicalCorner;

            var distance = this.pieceRemainingDistance();
            var piece = this.piece.next;
            while (!piece.logicalCorner || piece.logicalCorner == current) {
                distance += this.lane.laneAdjustedPieceDistance(piece);
                piece = piece.next;
            }
            return distance;
        }

        nextLogicalCorner(): LogicalCorner {
            var corner = this.piece.next;
            while (!corner.logicalCorner || corner.logicalCorner == this.piece.logicalCorner) {
                corner = corner.next;
            }
            return corner.logicalCorner;
        }
        // distance to next corner
        // next corner radius
        // highest manageable corner speed in this piece and lane

        percentageLapDoneApprox(): number {
            // note that lane choices affect this, use only as crude ballpark figure
            return this.inLapDistanceApprox() / this.lapDistanceApprox();
        }

        inLapDistanceApprox(): number {
            // note that lane choices affect this, use only as crude ballpark figure
            var distance = this.inPieceDistance;
            var piece = pieces[0];
            while (piece != this.piece) {
                distance += piece.pieceDistanceApprox();
                piece = piece.next;
            }
            return distance;
        }

        lapDistanceApprox(): number {
            // note that lane choices affect this, use only as crude ballpark figure
            var distance = 0;
            pieces.forEach(piece => distance += piece.pieceDistanceApprox());
            return distance;
        }
    }

    var lanes: Lane[] = [];
    var pieces: Piece[] = [];
    var logicalCorners: LogicalCorner[] = [];

    class State {

        gameTick: number;
        car: Car;
        location: Location;

        previous: State;
        previousPrevious: State;

        informCarPosition(gameTick: number, throttle: number, carPosition: API.CarPosition, turboActive: boolean) {
            // rotate past data
            if (gameTick > 0) {
                this.previousPrevious = this.previous;
                this.previous = this.clone();
            }

            this.gameTick = gameTick;
            this.car = new Car();
            this.location = new Location();

            // set given information
            this.car.angle = carPosition.angle;
            this.location.inPieceDistance = carPosition.piecePosition.inPieceDistance;
            this.car.throttle = throttle;
            this.car.turboActive = turboActive;

            this.location.piece = pieces[carPosition.piecePosition.pieceIndex];
            this.location.lane = lanes[carPosition.piecePosition.lane.startLaneIndex];

            // calculate delta information
            if (this.previous) {
                if (this.location.piece == this.previous.location.piece) {
                    // on same piece
                    this.car.speed = this.location.inPieceDistance - this.previous.location.inPieceDistance;
                } else {
                    // on new piece, what was left in previous piece needs to be included
                    this.car.speed = this.previous.location.pieceRemainingDistance() + this.location.inPieceDistance;
                }
                this.car.acceleration = this.car.speed - this.previous.car.speed;
                this.car.angleSpeed = this.car.angle - this.previous.car.angle;
                this.car.angleAcceleration = this.car.angleSpeed - this.previous.car.angleSpeed;
            }
        }

        onNewPiece() {
            return (this.previous && this.previous.location.piece.index != this.location.piece.index);
        }

        private clone() {
            var c = new State();
            c.gameTick = this.gameTick;
            c.car = _.clone(this.car);
            c.location = _.clone(this.location);
            return c;
        }
    }

    class CornerApproach {
        // aim
        magicNumber: number = 0;
        bonusFactor: number = 1;

        // results
        maxAngle: number = 0;
        speedAtMaxAngle: number = 0;
        angleAccelerationAtAngle: number = 0;
        crash: boolean = false;
    }

    class LogicalCorner {
        radius;
        angle;

        pieceBefore: Piece;

        approaches: CornerApproach[] = [];

        isPreviousPieceCornerInOppositeDirection(): boolean {
            return this.pieceBefore.isCorner() && sign(this.pieceBefore.info.angle) != sign(this.angle);
        }

        latestApproach() {
            return this.approaches[this.approaches.length - 1];
        }

        secondLatestApproach() {
            return this.approaches[this.approaches.length - 2];
        }
    }

    class Physics {

        // straight line a = kv + s

        k_inited = false;
        s_accel_inited = false;
        s_accel_turbo_inited = false;
        s_decel_inited = false;
        s_decel_turbo_inited = false;

        // initial guesses 
        k = -0.02;
        s_decel = 0;
        s_decel_turbo = 0;
        s_accel = 0.2;
        s_accel_turbo = 0;

        // angular acc
        //        a 

        // TODO: There can probably be different turbo factors in same race
        estimateMaxDeceleration(v: number, turbo: boolean): number {
            return this.k * v + ((turbo) ? this.s_decel_turbo : this.s_decel);
        }

        estimateMaxAcceleration(v: number, turbo: boolean): number {
            return this.k * v + ((turbo) ? this.s_accel_turbo : this.s_accel);
        }

        estimateTargetSpeed(logicalCorner: LogicalCorner, currentLane: Lane): number {
            //            console.log('lane adjusted radius:' + currentLane.laneAdjustedRadiusForThis(logicalCorner));
            return logicalCorner.latestApproach().bonusFactor * Math.sqrt(logicalCorner.latestApproach().magicNumber * currentLane.laneAdjustedRadiusForThis(logicalCorner));
        }

        adjustCornerApproach(logicalCorner: LogicalCorner, racing: boolean) {
            var approach = new CornerApproach();

            if (racing) {
                // lets not adjust anything when racing
                approach.magicNumber = logicalCorner.latestApproach().magicNumber;
                approach.bonusFactor = logicalCorner.latestApproach().bonusFactor;
            } else {
                if (!logicalCorner.latestApproach()) {
                    // a starting point
                    approach.magicNumber = 0.4;
                } else {
                    var previous = logicalCorner.latestApproach();
                    //                if (lap == 1) {
                    if (previous.crash) {
                        approach.magicNumber = 0.9 * previous.magicNumber;
                    } else {
                        if (Math.abs(previous.maxAngle) > 50) {
                            approach.magicNumber = previous.magicNumber;
                        } else if (Math.abs(previous.maxAngle) < 30) {
                            approach.magicNumber = 1.1 * previous.magicNumber;
                        } else {
                            approach.magicNumber = (1 + 0.1 * (1 - ((Math.abs(previous.maxAngle) - 30) / 20))) * previous.magicNumber;
                        }
                    }
                    //                } else {
                    //                    var previousPrevious = logicalCorner.takes[lap - 2];
                    //                    if (previous.crash) {
                    //                        take.magicNumber = (previous.magicNumber + previousPrevious.magicNumber) / 2;
                    //                    } else {
                    //                        take.magicNumber = 1.1 * previous.magicNumber;
                    //                    }
                    //                }
                }


                // if short segment, add a little
                // do a stepping here for 20 corner add more than for 40 corner, cut off not adding anything for 45 and so on..
                if (Math.abs(logicalCorner.angle) < 50) {
                    approach.bonusFactor *= 1 + 0.1 * (1 - Math.abs(logicalCorner.angle) / 50)
                }
                // if previous corner was to previous direction, add a little
                if (logicalCorner.isPreviousPieceCornerInOppositeDirection()) {
                    approach.bonusFactor *= 1.05;
                }
            }
            logicalCorner.approaches.push(approach);
        }


        inform(state: State) {
            if (state.previous == null || state.previousPrevious == null)
                return;

            this.calculateAcceleration(state);
            this.calculateCornering(state);
        }

        calculateCornering(state: State) {
            // if on new corner
            var endedCorner = state.previous.location.piece.logicalCorner;
            if (endedCorner) {
                if (!state.location.piece.logicalCorner || state.location.piece.logicalCorner != endedCorner) {
                    console.log("A logical corner done");
                    console.log("approach #: " + endedCorner.approaches.length);
                    console.log("radius: " + state.previous.location.laneAdjustedRadius());
                    console.log("angle: " + endedCorner.angle);
                    console.log("maxAngle: " + endedCorner.latestApproach().maxAngle);
                    console.log("angle speed at max angle: " + endedCorner.latestApproach().speedAtMaxAngle);
                    console.log("angle accel at max angle: " + endedCorner.latestApproach().angleAccelerationAtAngle);
                    console.log("magic number used: " + endedCorner.latestApproach().magicNumber);
                    console.log("bonuses number used: " + endedCorner.latestApproach().bonusFactor);
                    if (endedCorner.approaches.length >= 2) {
                        console.log("magic number adjusted: " + Util.round(endedCorner.latestApproach().magicNumber / endedCorner.secondLatestApproach().magicNumber * 100 - 100, 1) + "%");
                    }
                }
            }

            if (!state.location.piece.isCorner())
                return;

            var logicalCorner = state.location.piece.logicalCorner;
            if (logicalCorner.latestApproach().maxAngle < Math.abs(state.car.angle)) {
                logicalCorner.latestApproach().maxAngle = Math.abs(state.car.angle);
                logicalCorner.latestApproach().speedAtMaxAngle = Math.abs(state.car.angleSpeed);
                logicalCorner.latestApproach().angleAccelerationAtAngle = Math.abs(state.car.angleAcceleration);
            }
        }

        isAngleZero(state: State) {
            return Math.abs(state.car.angle) < 0.01;
        }

        calculateAcceleration(state: State) {
            // can't mix these, calculations not possible
            if (state.previousPrevious.car.turboActive != state.previous.car.turboActive)
                return;
            if (state.previous.car.throttle != state.car.throttle)
                return;
            if (sign(state.car.acceleration) != sign(state.car.acceleration))
                return;
            // want these only at straights
            if (!this.isAngleZero(state.previousPrevious) || !this.isAngleZero(state.previous) || !this.isAngleZero(state)) {
                //                console.log('Angles screwed');
                return;
            }
            if (state.previousPrevious.location.piece.isCorner() || state.previous.location.piece.isCorner() || state.location.piece.isCorner()) {
                //                console.log('Not in straight line');
                return;
            }
            //
            //            if (state.car.acceleration == 0)
            //                return;


            var v0 = state.previousPrevious.car.speed;
            var v1 = state.previous.car.speed;
            var v2 = state.car.speed;

            var a1 = state.previous.car.acceleration;
            var a2 = state.car.acceleration;

            var k = (a2 - a1) / (v2 - v1);
            var s = a1 - k * v1;

            if (this.k_inited) {
                this.reportWayOff('k', this.k, k);
            } else {
                console.log('Learned k: ' + k);
                this.k_inited = true;
                this.k = k;
            }

            if (state.previous.car.throttle == 0) {
                if (state.previous.car.turboActive) {
                    if (this.s_decel_turbo_inited) {
                        this.reportWayOff('s_decel_turbo', this.s_decel_turbo, s);
                    } else {
                        console.log('Learned decel turbo s: ' + s);
                        this.s_decel_turbo_inited = true;
                        this.s_decel_turbo = s;
                    }
                } else {
                    if (this.s_decel_inited) {
                        this.reportWayOff('s_decel', this.s_decel, s);
                    } else {
                        console.log('Learned decel s: ' + s);
                        this.s_decel_inited = true;
                        this.s_decel = s;
                    }
                }
            }
            if (state.previous.car.throttle == 1) {
                if (state.previous.car.turboActive) {
                    if (this.s_accel_turbo_inited) {
                        this.reportWayOff('s_accel_turbo', this.s_accel_turbo, s);
                    } else {
                        console.log('Learned accel turbo s: ' + s);
                        this.s_accel_turbo_inited = true;
                        this.s_accel_turbo = s;
                    }
                } else {
                    if (this.s_accel_inited) {
                        this.reportWayOff('s_accel', this.s_accel, s);
                    } else {
                        console.log('Learned accel s: ' + s);
                        this.s_accel_inited = true;
                        this.s_accel = s;
                    }
                }
            }
        }

        reportWayOff(varName, varValue, real) {
            if (Math.abs(real) > 0.00000001 && Math.abs(varValue / real - 1) > 0.1) {
                console.log(varName + ' (' + varValue + ') way off from current reading:' + real);
            }
        }
    }



    export enum NextAction {
        TURBO,
        SWITCH_LANE_LEFT,
        SWITCH_LANE_RIGHT,
        THROTTLE
    }

    var lastLogLines = [];
    export class Engine {

        public nextAction: NextAction;
        public throttle: number = 0;

        private gameId;
        private gameTick = 0;
        private yourCar: API.CarId;
        private state = new State();
        private adjustCorners = true;

        private turboAvailable: API.Turbo;
        private turboRequestedWaitingForActivation: API.Turbo;
        private turboActive: API.Turbo;
        private turboActivatedOnTick: number;

        private physics = new Physics();

        constructor() {
            //            CSV.open();
        }

        informJoin(join: API.Join) {
            console.log('Joined')
        }

        informJoinRace(join: API.Join) {
            console.log('Joined race')
        }

        informGameId(gameId: string) {
            console.log('Game id: ' + gameId);
            this.gameId = gameId;
        }

        informGameTick(gameTick: number) {
            this.gameTick = gameTick;
        }

        informYourCar(yourCar: API.CarId) {
            this.yourCar = yourCar;
            console.log('Riding in: ' + yourCar.color);
        }

        private gameInited = false;
        informGameInit(gameInit: API.GameInit) {
            if (this.gameInited) {
                return;
            }
            this.gameInited = true;
            // pieces
            for (var i = 0; i < gameInit.race.track.pieces.length; i++) {
                pieces[i] = new Piece();
                pieces[i].info = gameInit.race.track.pieces[i];
                pieces[i].index = i;
            }
            // link next piece info
            for (var i = 0; i < pieces.length; i++) {
                pieces[i].next = pieces[(i + 1) % pieces.length];
            }
            // combine same continuous corners into LogicalCorners
            pieces.forEach((piece) => {
                if (piece.isCorner() && piece.logicalCorner == null) {
                    var logicalCorner = new LogicalCorner();
                    logicalCorners.push(logicalCorner);
                    logicalCorner.radius = piece.info.radius;
                    logicalCorner.pieceBefore = piece.pieceBefore();
                    //                    logicalCorner.angle = piece.info.angle;
                    logicalCorner.angle = 0;
                    var followingPiece = piece;
                    do {
                        logicalCorner.angle += followingPiece.info.angle;
                        followingPiece.logicalCorner = logicalCorner;
                        followingPiece = followingPiece.next;
                    } while (followingPiece.info.radius == piece.info.radius && sign(followingPiece.info.angle) == sign(piece.info.angle))
                }
            });
            // lanes
            for (var i = 0; i < gameInit.race.track.lanes.length; i++) {
                var lane = gameInit.race.track.lanes[i];
                lanes[lane.index] = new Lane();
                lanes[lane.index].info = lane;
            }
            // calculate best turbo place, i.e beginning of longest straight
            this.longestStraightStart().suitableForTurbo = true;
            // now, these guys are really something, starting in the middle of a corner
            if (pieces[0].logicalCorner) {
                this.physics.adjustCornerApproach(pieces[0].logicalCorner, this.racing());
            }
        }

        private longestStraightStart() {
            var longestStraightStart: Piece;

            pieces.forEach((piece) => {
                if (longestStraightStart == null || piece.consecutiveStraights() > longestStraightStart.consecutiveStraights())
                    longestStraightStart = piece;
            });

            return longestStraightStart;
        }

        private gameStarts = 0;
        informGameStart(gameStart: API.GameStart) {
            this.gameStarts++;
            console.log('Race started');
            if (this.racing()) {
                console.log('NOW RACING !!!!!!!!!!!!!!!!!!!');
            }
        }

        racing() {
            return this.gameStarts > 1;
        }

        informTurboAvailable(turbo: API.Turbo) {
            //            console.log('Turbo available');
            if (this.turboAvailable) {
                //                console.log("Already got one turbo available. Supposed to get two? Code some handling for this.");
                // there probably arent two turbos?
            } else {
                this.turboAvailable = turbo;
            }
        }

        informTurboStart(car: API.CarId) {
            if (this.ourCar(car)) {
                this.turboActive = this.turboRequestedWaitingForActivation;
                this.turboActivatedOnTick = this.gameTick;
                this.turboRequestedWaitingForActivation = null;
            }
        }

        informTurboEnd(car: API.CarId) {
            if (this.ourCar(car)) {
                this.turboActive = null;
                this.turboActivatedOnTick = null;
            }
        }

        ourCar(car: API.CarId) {
            return car.color == this.yourCar.color;
        }

        informCrash(car: API.CarId) {
            if (this.ourCar(car)) {
                //                console.log('');
                console.log('**** CRASH ****');
                console.log('Went off at angle: ' + this.state.car.angle);
                if (this.state.location.piece.logicalCorner) {
                    this.state.location.piece.logicalCorner.latestApproach().crash = true;
                }
            }
        }

        private lapTimes = [];
        informLapFinished(lapFinished: API.LapFinished) {
            this.lapTimes.push(lapFinished.lapTime.millis);
            console.log("LAP TIMES: " + this.lapTimes.join(" -> "));
        }

        informGameEnd(gameEnd: API.GameEnd) {
            //            CSV.close();
            console.log('Race ended');
        }

        informTournamentEnd(tournamentEnd) {
            console.log('Watch replay at: https://helloworldopen.com/race-visualizer/?recording=https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/' + this.gameId + '.json');
        }

        private getOurCar(carPositions: API.CarPosition[]): API.CarPosition {
            for (var i = 0; i < carPositions.length; i++) {
                var carPosition = carPositions[i];
                if (carPosition.id.color == this.yourCar.color) {
                    return carPosition;
                }
            }
        }

        informCarPositions(carPositions: API.CarPosition[]) {
            var ourCar = this.getOurCar(carPositions);
            this.state.informCarPosition(this.gameTick, this.throttle, ourCar, this.turboActive != null);
            this.physics.inform(this.state);

            this.printHuman();
            this.printCSV();

            this.decideNextAction();
        }

        printHuman() {
            var precision = 1;
            var line = this.gameTick +
                '\t' +
                (this.state.car.turboActive ? "T " : "  ") +
                Util.roundPad(this.state.car.throttle, 1) + ' => ' +
                Util.roundPad(this.state.car.speed, 2) + ' (' +
                Util.signPrefixRoundPad(this.state.car.acceleration, 3) + ')'
                + '\t' +
                ' = ' + Util.whiteFrontPad(Util.round(this.state.car.angle, 1), 4) + ' = '
                + '\t'
                + Util.whiteFrontPad(Util.roundPad(this.state.location.percentageLapDoneApprox(), 3), 4) + " (" + Util.whiteFrontPad(this.state.location.piece.index, 2) + ") " + (this.state.location.piece.isCorner() ? " C " : "   ")
                + (this.turboAvailable ? "(T:" + this.turboAvailable.turboDurationTicks + ":x" + this.turboAvailable.turboFactor + ") " : "")
                + (this.state.location.piece.isCorner() ? "(log. a: " + this.state.location.piece.logicalCorner.angle + " r: " + this.state.location.piece.logicalCorner.radius + ")" : "");

            lastLogLines.push(line);
            if (lastLogLines.length > 10) {
                lastLogLines.splice(0, 1);
            }
            console.log(line);

        }

        printCSV() {
            var csv = [];

            csv.push(this.gameTick);
            csv.push(Util.round(this.state.car.speed, 6));
            csv.push(Util.round(this.state.car.acceleration, 6));
            csv.push(this.state.car.turboActive);
            csv.push(Util.round(this.throttle, 6));
            csv.push(Util.round(this.state.car.angle, 6));
            csv.push(Util.round(this.state.car.angleSpeed, 6));
            csv.push(Util.round(this.state.car.angleAcceleration, 6));
            csv.push(Util.nvl(this.state.location.laneAdjustedRadius(), 0));

            //            CSV.write(csv.join('\t'));
        }

        decideNextAction() {
            this.nextAction = null;
            if (this.nextAction == null) {
                this.decideTurbo();
            }
            if (this.nextAction == null) {
                this.decideSwitchLane();
            }
            if (this.nextAction == null) {
                this.decideThrottle();
            }
        }

        decideTurbo() {
            if (!this.turboRequestedWaitingForActivation && !this.turboActive) {
                if (this.turboAvailable && this.state.location.piece.suitableForTurbo) {
                    this.nextAction = NextAction.TURBO;
                    this.turboRequestedWaitingForActivation = this.turboAvailable;
                    this.turboAvailable = null;
                }
            }
        }

        decideSwitchLane() {
            // every 1000th tick at random
            if (this.racing() && Math.random() < 0.01) {
                if (this.state.location.lane.canSwitchLeft() && Math.random() < 0.5) {
                    this.nextAction = NextAction.SWITCH_LANE_LEFT;
                } else if (this.state.location.lane.canSwitchRight() && Math.random() < 0.5) {
                    this.nextAction = NextAction.SWITCH_LANE_RIGHT;
                }
            }
            //            this.nextAction = NextAction.SWITCH_LANE
            // now, make sure you are not doing anything like this when you already have requested turbo
        }

        decideThrottle() {
            this.nextAction = NextAction.THROTTLE;
            this.throttle = this.getThrottle();
        }


        private nextLogicalCorner: LogicalCorner;
        private getThrottle(): number {
            // starting quali at middle of corner causes some trouble {
            if (this.gameTick == 0 && this.state.location.piece.logicalCorner) {
                this.physics.adjustCornerApproach(this.state.location.piece.logicalCorner, this.racing());
            }

            var onStraight = this.state.location.piece.isStraight();

            var distanceToNextLogicalCorner = this.state.location.distanceToNextLogicalCorner();

            // new corner
            if (this.state.location.nextLogicalCorner() != this.nextLogicalCorner) {
                this.nextLogicalCorner = this.state.location.nextLogicalCorner();
                this.physics.adjustCornerApproach(this.nextLogicalCorner, this.racing());
            }
            var nextCornerTargetSpeed = this.physics.estimateTargetSpeed(this.nextLogicalCorner, this.state.location.lane);
            var breakingDistanceToNextCorner = this.estimateBreakingDistanceToSpeed(nextCornerTargetSpeed);

            //            console.log('Distance to next logical corner: ' + distanceToNextLogicalCorner);
            //            console.log('Next logical corner target speed: ' + nextCornerTargetSpeed);
            //            console.log('Estimated breaking distance: ' + breakingDistanceToNextCorner);

            if (onStraight) {
                // in straight

                if (distanceToNextLogicalCorner > breakingDistanceToNextCorner) {

                    // - freeway
                    return 1;
                } else {
                    // - breaking zone
                    return this.adjustThrottleForTargetSpeed(nextCornerTargetSpeed);
                }
            } else {
                // in corner
                var currentCornerTargetSpeed = this.physics.estimateTargetSpeed(this.state.location.piece.logicalCorner, this.state.location.lane);
                //                console.log('Current logical corner target speed: ' + currentCornerTargetSpeed);

                if (distanceToNextLogicalCorner > breakingDistanceToNextCorner) {
                    // - cornering
                    return this.adjustThrottleForTargetSpeed(currentCornerTargetSpeed);
                } else {
                    // - breaking zone
                    return this.adjustThrottleForTargetSpeed(Math.min(currentCornerTargetSpeed, nextCornerTargetSpeed));
                }
            }
        }

        turboDurationLeft() {
            if (this.turboActive) {
                return this.turboActive.turboDurationTicks - (this.gameTick - this.turboActivatedOnTick);
            } else {
                return 0;
            }
        }

        private estimateBreakingDistanceToSpeed(targetSpeed: number): number {
            var simulatedCurrentSpeed = this.state.car.speed;
            var simulatedTurboDurationLeft = this.turboDurationLeft();
            var simulatedDistanceTravelled = 0;
            while (simulatedCurrentSpeed > targetSpeed) {
                simulatedDistanceTravelled += simulatedCurrentSpeed;
                simulatedCurrentSpeed += this.physics.estimateMaxDeceleration(simulatedCurrentSpeed, simulatedTurboDurationLeft > 0);
                simulatedTurboDurationLeft--;
            }
            return simulatedDistanceTravelled;
        }

        private adjustThrottleForTargetSpeed(targetSpeed: number): number {
            var currentSpeed = this.state.car.speed;
            var speedDifference = targetSpeed - currentSpeed;
            var turbo = this.state.car.turboActive;
            if (speedDifference < 0) {
                var maxDec = this.physics.estimateMaxDeceleration(currentSpeed, turbo);
                return Math.max(0, 1 - speedDifference / maxDec);
            } else {
                var maxAcc = this.physics.estimateMaxAcceleration(currentSpeed, turbo);
                return Math.min(1, speedDifference / maxAcc);
            }
        }
    }
}

export = Engine; 