var _ = require("../lib/underscore");

var Util;
(function (Util) {
    function stringify(obj) {
        return JSON.stringify(obj, null, 4);
    }
    Util.stringify = stringify;

    function round(v, d) {
        return Math.round(v * Math.pow(10, d)) / Math.pow(10, d);
    }
    Util.round = round;

    function whiteFrontPad(s, w) {
        s = s + "";
        while (s.length < w)
            s = " " + s;
        return s;
    }
    Util.whiteFrontPad = whiteFrontPad;

    function roundPad(v, w) {
        var value = round(v, w) + "";
        if (!/\./.test(value)) {
            value += ".";
            while (w-- > 0) {
                value += "0";
            }
        } else {
            while (value.substring(value.indexOf('.') + 1).length < w)
                value += "0";
        }

        return value;
    }
    Util.roundPad = roundPad;

    function signPrefixRoundPad(v, w) {
        var value = roundPad(v, w - 1);
        return ((v > 0) ? "+" : "") + value;
    }
    Util.signPrefixRoundPad = signPrefixRoundPad;

    function debug(obj) {
        if (!_.isString(obj))
            obj = stringify(obj);
        console.log('DEBUG: ' + obj);
    }
    Util.debug = debug;

    function nvl(value, alternative) {
        return (value) ? value : alternative;
    }
    Util.nvl = nvl;
})(Util || (Util = {}));

module.exports = Util;
