/// <reference path="node.d.ts"/>
/// <reference path="underscore.d.ts"/>
/// <reference path="moment.d.ts"/>
var net = require("net");
var JSONStream = require('JSONStream');

var Engine = require('./engine');
var Util = require('./util');

var serverHost = process.argv[2];
var serverPort = parseInt(process.argv[3]);
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var client = net.connect(serverPort, serverHost, function () {
    var join = {
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    };
    var joinRace = {
        "msgType": "joinRace",
        "data": {
            "botId": {
                "name": botName,
                "key": botKey
            },
            "trackName": "keimola",
            "password": "123",
            "carCount": 4
        }
    };
    return send(join);
});

function send(json) {
    //    console.log('-> ' + json.msgType);
    client.write(JSON.stringify(json));
    return client.write('\n');
}

function ping() {
    send({
        msgType: "ping",
        data: null
    });
}

var jsonStream = client.pipe(JSONStream.parse());
var engine = new Engine.Engine();

jsonStream.on('data', function (data) {
    //    console.log('<- ' + data.msgType);
    if ('gameTick' in data) {
        engine.informGameTick(data.gameTick);
    }

    switch (data.msgType) {
        case 'join':
            engine.informJoin(data.data);
            break;
        case 'joinRace':
            engine.informJoinRace(data.data);
            break;
        case 'yourCar':
            engine.informYourCar(data.data);
            break;
        case 'gameInit':
            engine.informGameInit(data.data);
            break;
        case 'carPositions':
            if (!('gameTick' in data)) {
                //                Util.debug(data);
            }
            engine.informCarPositions(data.data);
            break;
        case 'gameStart':
            engine.informGameStart(data.data);
            engine.informGameId(data.gameId);
            ping();
            break;
        case 'crash':
            engine.informCrash(data.data);
            break;
        case 'spawn':
            break;
        case 'turboAvailable':
            engine.informTurboAvailable(data.data);
            break;
        case 'turboStart':
            engine.informTurboStart(data.data);
            break;
        case 'turboEnd':
            engine.informTurboEnd(data.data);
            break;
        case 'gameEnd':
            engine.informGameEnd(data.data);
            break;
        case 'lapFinished':
            engine.informLapFinished(data.data);
            break;
        case 'finish':
            break;
        case 'tournamentEnd':
            engine.informTournamentEnd(data.data);
            break;
        default:
            console.log();
            console.log("********** UNKNOWN SERVER RESPONSE **********");
            console.log(Util.stringify(data));
            console.log();
    }

    // respond to certain messages
    if (data.msgType == 'carPositions') {
        if (engine.nextAction == 0 /* TURBO */) {
            //            console.log('Using turbo' + data.gameTick);
            send({
                msgType: "turbo",
                data: "Let off some steam, Bennett!",
                gameTick: data.gameTick
            });
        } else if (engine.nextAction == 1 /* SWITCH_LANE_LEFT */) {
            send({
                msgType: "switchLane",
                data: "Left",
                gameTick: data.gameTick
            });
        } else if (engine.nextAction == 2 /* SWITCH_LANE_RIGHT */) {
            send({
                msgType: "switchLane",
                data: "Right",
                gameTick: data.gameTick
            });
        } else if (engine.nextAction == 3 /* THROTTLE */) {
            send({
                msgType: "throttle",
                data: Math.max(Math.min(engine.throttle, 1), 0),
                gameTick: data.gameTick
            });
        } else {
            throw new Error("WTF?");
            /*
            send({
            msgType: "ping",
            data: {}
            });
            */
        }
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});
