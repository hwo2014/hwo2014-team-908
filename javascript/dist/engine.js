var _ = require("../lib/underscore");

var Util = require("./util");

var Engine;
(function (_Engine) {
    var Lane = (function () {
        function Lane() {
        }
        Lane.prototype.laneAdjustedRadiusForThis2 = function (radius, angle) {
            return radius - sign(angle) * this.info.distanceFromCenter;
        };

        Lane.prototype.laneAdjustedRadiusForThis = function (logicalCorner) {
            return this.laneAdjustedRadiusForThis2(logicalCorner.radius, logicalCorner.angle);
        };

        Lane.prototype.laneAdjustedRadius = function (piece) {
            return this.laneAdjustedRadiusForThis2(piece.info.radius, piece.info.angle);
        };

        Lane.prototype.laneAdjustedPieceDistance = function (piece) {
            if (piece.isCorner()) {
                return Math.abs(piece.info.angle) / 360.0 * 2 * Math.PI * this.laneAdjustedRadius(piece);
            } else {
                return piece.info.length;
            }
        };

        Lane.prototype.canSwitchLeft = function () {
            return this.info.index > 0;
        };

        Lane.prototype.canSwitchRight = function () {
            return this.info.index < (lanes.length - 1);
        };
        return Lane;
    })();

    var Piece = (function () {
        function Piece() {
        }
        Piece.prototype.isCorner = function () {
            return 'radius' in this.info;
        };

        Piece.prototype.isStraight = function () {
            return 'length' in this.info;
        };

        Piece.prototype.consecutiveStraights = function () {
            var count = 0;
            var p = this;
            while (p.isStraight()) {
                count++;
                p = p.next;
            }
            return count;
        };

        Piece.prototype.pieceDistanceApprox = function () {
            if (this.isCorner()) {
                // lane changes things when in corner, use this only for fyi information
                return Math.abs(this.info.angle) / 360.0 * 2 * Math.PI;
            } else {
                return this.info.length;
            }
        };

        Piece.prototype.pieceBefore = function () {
            var before = this;
            while (before.next != this)
                before = before.next;
            return before;
        };
        return Piece;
    })();

    var Car = (function () {
        function Car() {
            this.speed = 0;
            this.acceleration = 0;
            this.angle = 0;
            this.angleSpeed = 0;
            this.angleAcceleration = 0;
        }
        return Car;
    })();

    function sign(n) {
        return n < 0 ? -1 : 1;
    }

    var Location = (function () {
        function Location() {
        }
        Location.prototype.laneAdjustedRadius = function () {
            return this.lane.laneAdjustedRadius(this.piece);
        };

        Location.prototype.laneAdjustedPieceDistance = function () {
            return this.lane.laneAdjustedPieceDistance(this.piece);
        };

        Location.prototype.pieceRemainingDistance = function () {
            return this.laneAdjustedPieceDistance() - this.inPieceDistance;
        };

        Location.prototype.distanceToNextLogicalCorner = function () {
            var current = this.piece.logicalCorner;

            var distance = this.pieceRemainingDistance();
            var piece = this.piece.next;
            while (!piece.logicalCorner || piece.logicalCorner == current) {
                distance += this.lane.laneAdjustedPieceDistance(piece);
                piece = piece.next;
            }
            return distance;
        };

        Location.prototype.nextLogicalCorner = function () {
            var corner = this.piece.next;
            while (!corner.logicalCorner || corner.logicalCorner == this.piece.logicalCorner) {
                corner = corner.next;
            }
            return corner.logicalCorner;
        };

        // distance to next corner
        // next corner radius
        // highest manageable corner speed in this piece and lane
        Location.prototype.percentageLapDoneApprox = function () {
            // note that lane choices affect this, use only as crude ballpark figure
            return this.inLapDistanceApprox() / this.lapDistanceApprox();
        };

        Location.prototype.inLapDistanceApprox = function () {
            // note that lane choices affect this, use only as crude ballpark figure
            var distance = this.inPieceDistance;
            var piece = pieces[0];
            while (piece != this.piece) {
                distance += piece.pieceDistanceApprox();
                piece = piece.next;
            }
            return distance;
        };

        Location.prototype.lapDistanceApprox = function () {
            // note that lane choices affect this, use only as crude ballpark figure
            var distance = 0;
            pieces.forEach(function (piece) {
                return distance += piece.pieceDistanceApprox();
            });
            return distance;
        };
        return Location;
    })();

    var lanes = [];
    var pieces = [];
    var logicalCorners = [];

    var State = (function () {
        function State() {
        }
        State.prototype.informCarPosition = function (gameTick, throttle, carPosition, turboActive) {
            // rotate past data
            if (gameTick > 0) {
                this.previousPrevious = this.previous;
                this.previous = this.clone();
            }

            this.gameTick = gameTick;
            this.car = new Car();
            this.location = new Location();

            // set given information
            this.car.angle = carPosition.angle;
            this.location.inPieceDistance = carPosition.piecePosition.inPieceDistance;
            this.car.throttle = throttle;
            this.car.turboActive = turboActive;

            this.location.piece = pieces[carPosition.piecePosition.pieceIndex];
            this.location.lane = lanes[carPosition.piecePosition.lane.startLaneIndex];

            // calculate delta information
            if (this.previous) {
                if (this.location.piece == this.previous.location.piece) {
                    // on same piece
                    this.car.speed = this.location.inPieceDistance - this.previous.location.inPieceDistance;
                } else {
                    // on new piece, what was left in previous piece needs to be included
                    this.car.speed = this.previous.location.pieceRemainingDistance() + this.location.inPieceDistance;
                }
                this.car.acceleration = this.car.speed - this.previous.car.speed;
                this.car.angleSpeed = this.car.angle - this.previous.car.angle;
                this.car.angleAcceleration = this.car.angleSpeed - this.previous.car.angleSpeed;
            }
        };

        State.prototype.onNewPiece = function () {
            return (this.previous && this.previous.location.piece.index != this.location.piece.index);
        };

        State.prototype.clone = function () {
            var c = new State();
            c.gameTick = this.gameTick;
            c.car = _.clone(this.car);
            c.location = _.clone(this.location);
            return c;
        };
        return State;
    })();

    var CornerApproach = (function () {
        function CornerApproach() {
            // aim
            this.magicNumber = 0;
            this.bonusFactor = 1;
            // results
            this.maxAngle = 0;
            this.speedAtMaxAngle = 0;
            this.angleAccelerationAtAngle = 0;
            this.crash = false;
        }
        return CornerApproach;
    })();

    var LogicalCorner = (function () {
        function LogicalCorner() {
            this.approaches = [];
        }
        LogicalCorner.prototype.isPreviousPieceCornerInOppositeDirection = function () {
            return this.pieceBefore.isCorner() && sign(this.pieceBefore.info.angle) != sign(this.angle);
        };

        LogicalCorner.prototype.latestApproach = function () {
            return this.approaches[this.approaches.length - 1];
        };

        LogicalCorner.prototype.secondLatestApproach = function () {
            return this.approaches[this.approaches.length - 2];
        };
        return LogicalCorner;
    })();

    var Physics = (function () {
        function Physics() {
            // straight line a = kv + s
            this.k_inited = false;
            this.s_accel_inited = false;
            this.s_accel_turbo_inited = false;
            this.s_decel_inited = false;
            this.s_decel_turbo_inited = false;
            // initial guesses
            this.k = -0.02;
            this.s_decel = 0;
            this.s_decel_turbo = 0;
            this.s_accel = 0.2;
            this.s_accel_turbo = 0;
        }
        // angular acc
        //        a
        // TODO: There can probably be different turbo factors in same race
        Physics.prototype.estimateMaxDeceleration = function (v, turbo) {
            return this.k * v + ((turbo) ? this.s_decel_turbo : this.s_decel);
        };

        Physics.prototype.estimateMaxAcceleration = function (v, turbo) {
            return this.k * v + ((turbo) ? this.s_accel_turbo : this.s_accel);
        };

        Physics.prototype.estimateTargetSpeed = function (logicalCorner, currentLane) {
            //            console.log('lane adjusted radius:' + currentLane.laneAdjustedRadiusForThis(logicalCorner));
            return logicalCorner.latestApproach().bonusFactor * Math.sqrt(logicalCorner.latestApproach().magicNumber * currentLane.laneAdjustedRadiusForThis(logicalCorner));
        };

        Physics.prototype.adjustCornerApproach = function (logicalCorner, racing) {
            var approach = new CornerApproach();

            if (racing) {
                // lets not adjust anything when racing
                approach.magicNumber = logicalCorner.latestApproach().magicNumber;
                approach.bonusFactor = logicalCorner.latestApproach().bonusFactor;
            } else {
                if (!logicalCorner.latestApproach()) {
                    // a starting point
                    approach.magicNumber = 0.4;
                } else {
                    var previous = logicalCorner.latestApproach();

                    //                if (lap == 1) {
                    if (previous.crash) {
                        approach.magicNumber = 0.9 * previous.magicNumber;
                    } else {
                        if (Math.abs(previous.maxAngle) > 50) {
                            approach.magicNumber = previous.magicNumber;
                        } else if (Math.abs(previous.maxAngle) < 30) {
                            approach.magicNumber = 1.1 * previous.magicNumber;
                        } else {
                            approach.magicNumber = (1 + 0.1 * (1 - ((Math.abs(previous.maxAngle) - 30) / 20))) * previous.magicNumber;
                        }
                    }
                    //                } else {
                    //                    var previousPrevious = logicalCorner.takes[lap - 2];
                    //                    if (previous.crash) {
                    //                        take.magicNumber = (previous.magicNumber + previousPrevious.magicNumber) / 2;
                    //                    } else {
                    //                        take.magicNumber = 1.1 * previous.magicNumber;
                    //                    }
                    //                }
                }

                // if short segment, add a little
                // do a stepping here for 20 corner add more than for 40 corner, cut off not adding anything for 45 and so on..
                if (Math.abs(logicalCorner.angle) < 50) {
                    approach.bonusFactor *= 1 + 0.1 * (1 - Math.abs(logicalCorner.angle) / 50);
                }

                // if previous corner was to previous direction, add a little
                if (logicalCorner.isPreviousPieceCornerInOppositeDirection()) {
                    approach.bonusFactor *= 1.05;
                }
            }
            logicalCorner.approaches.push(approach);
        };

        Physics.prototype.inform = function (state) {
            if (state.previous == null || state.previousPrevious == null)
                return;

            this.calculateAcceleration(state);
            this.calculateCornering(state);
        };

        Physics.prototype.calculateCornering = function (state) {
            // if on new corner
            var endedCorner = state.previous.location.piece.logicalCorner;
            if (endedCorner) {
                if (!state.location.piece.logicalCorner || state.location.piece.logicalCorner != endedCorner) {
                    console.log("A logical corner done");
                    console.log("approach #: " + endedCorner.approaches.length);
                    console.log("radius: " + state.previous.location.laneAdjustedRadius());
                    console.log("angle: " + endedCorner.angle);
                    console.log("maxAngle: " + endedCorner.latestApproach().maxAngle);
                    console.log("angle speed at max angle: " + endedCorner.latestApproach().speedAtMaxAngle);
                    console.log("angle accel at max angle: " + endedCorner.latestApproach().angleAccelerationAtAngle);
                    console.log("magic number used: " + endedCorner.latestApproach().magicNumber);
                    console.log("bonuses number used: " + endedCorner.latestApproach().bonusFactor);
                    if (endedCorner.approaches.length >= 2) {
                        console.log("magic number adjusted: " + Util.round(endedCorner.latestApproach().magicNumber / endedCorner.secondLatestApproach().magicNumber * 100 - 100, 1) + "%");
                    }
                }
            }

            if (!state.location.piece.isCorner())
                return;

            var logicalCorner = state.location.piece.logicalCorner;
            if (logicalCorner.latestApproach().maxAngle < Math.abs(state.car.angle)) {
                logicalCorner.latestApproach().maxAngle = Math.abs(state.car.angle);
                logicalCorner.latestApproach().speedAtMaxAngle = Math.abs(state.car.angleSpeed);
                logicalCorner.latestApproach().angleAccelerationAtAngle = Math.abs(state.car.angleAcceleration);
            }
        };

        Physics.prototype.isAngleZero = function (state) {
            return Math.abs(state.car.angle) < 0.01;
        };

        Physics.prototype.calculateAcceleration = function (state) {
            // can't mix these, calculations not possible
            if (state.previousPrevious.car.turboActive != state.previous.car.turboActive)
                return;
            if (state.previous.car.throttle != state.car.throttle)
                return;
            if (sign(state.car.acceleration) != sign(state.car.acceleration))
                return;

            // want these only at straights
            if (!this.isAngleZero(state.previousPrevious) || !this.isAngleZero(state.previous) || !this.isAngleZero(state)) {
                //                console.log('Angles screwed');
                return;
            }
            if (state.previousPrevious.location.piece.isCorner() || state.previous.location.piece.isCorner() || state.location.piece.isCorner()) {
                //                console.log('Not in straight line');
                return;
            }

            //
            //            if (state.car.acceleration == 0)
            //                return;
            var v0 = state.previousPrevious.car.speed;
            var v1 = state.previous.car.speed;
            var v2 = state.car.speed;

            var a1 = state.previous.car.acceleration;
            var a2 = state.car.acceleration;

            var k = (a2 - a1) / (v2 - v1);
            var s = a1 - k * v1;

            if (this.k_inited) {
                this.reportWayOff('k', this.k, k);
            } else {
                console.log('Learned k: ' + k);
                this.k_inited = true;
                this.k = k;
            }

            if (state.previous.car.throttle == 0) {
                if (state.previous.car.turboActive) {
                    if (this.s_decel_turbo_inited) {
                        this.reportWayOff('s_decel_turbo', this.s_decel_turbo, s);
                    } else {
                        console.log('Learned decel turbo s: ' + s);
                        this.s_decel_turbo_inited = true;
                        this.s_decel_turbo = s;
                    }
                } else {
                    if (this.s_decel_inited) {
                        this.reportWayOff('s_decel', this.s_decel, s);
                    } else {
                        console.log('Learned decel s: ' + s);
                        this.s_decel_inited = true;
                        this.s_decel = s;
                    }
                }
            }
            if (state.previous.car.throttle == 1) {
                if (state.previous.car.turboActive) {
                    if (this.s_accel_turbo_inited) {
                        this.reportWayOff('s_accel_turbo', this.s_accel_turbo, s);
                    } else {
                        console.log('Learned accel turbo s: ' + s);
                        this.s_accel_turbo_inited = true;
                        this.s_accel_turbo = s;
                    }
                } else {
                    if (this.s_accel_inited) {
                        this.reportWayOff('s_accel', this.s_accel, s);
                    } else {
                        console.log('Learned accel s: ' + s);
                        this.s_accel_inited = true;
                        this.s_accel = s;
                    }
                }
            }
        };

        Physics.prototype.reportWayOff = function (varName, varValue, real) {
            if (Math.abs(real) > 0.00000001 && Math.abs(varValue / real - 1) > 0.1) {
                console.log(varName + ' (' + varValue + ') way off from current reading:' + real);
            }
        };
        return Physics;
    })();

    (function (NextAction) {
        NextAction[NextAction["TURBO"] = 0] = "TURBO";
        NextAction[NextAction["SWITCH_LANE_LEFT"] = 1] = "SWITCH_LANE_LEFT";
        NextAction[NextAction["SWITCH_LANE_RIGHT"] = 2] = "SWITCH_LANE_RIGHT";
        NextAction[NextAction["THROTTLE"] = 3] = "THROTTLE";
    })(_Engine.NextAction || (_Engine.NextAction = {}));
    var NextAction = _Engine.NextAction;

    var lastLogLines = [];
    var Engine = (function () {
        function Engine() {
            this.throttle = 0;
            this.gameTick = 0;
            this.state = new State();
            this.adjustCorners = true;
            this.physics = new Physics();
            this.gameInited = false;
            this.gameStarts = 0;
            this.lapTimes = [];
            //            CSV.open();
        }
        Engine.prototype.informJoin = function (join) {
            console.log('Joined');
        };

        Engine.prototype.informJoinRace = function (join) {
            console.log('Joined race');
        };

        Engine.prototype.informGameId = function (gameId) {
            console.log('Game id: ' + gameId);
            this.gameId = gameId;
        };

        Engine.prototype.informGameTick = function (gameTick) {
            this.gameTick = gameTick;
        };

        Engine.prototype.informYourCar = function (yourCar) {
            this.yourCar = yourCar;
            console.log('Riding in: ' + yourCar.color);
        };

        Engine.prototype.informGameInit = function (gameInit) {
            if (this.gameInited) {
                return;
            }
            this.gameInited = true;

            for (var i = 0; i < gameInit.race.track.pieces.length; i++) {
                pieces[i] = new Piece();
                pieces[i].info = gameInit.race.track.pieces[i];
                pieces[i].index = i;
            }

            for (var i = 0; i < pieces.length; i++) {
                pieces[i].next = pieces[(i + 1) % pieces.length];
            }

            // combine same continuous corners into LogicalCorners
            pieces.forEach(function (piece) {
                if (piece.isCorner() && piece.logicalCorner == null) {
                    var logicalCorner = new LogicalCorner();
                    logicalCorners.push(logicalCorner);
                    logicalCorner.radius = piece.info.radius;
                    logicalCorner.pieceBefore = piece.pieceBefore();

                    //                    logicalCorner.angle = piece.info.angle;
                    logicalCorner.angle = 0;
                    var followingPiece = piece;
                    do {
                        logicalCorner.angle += followingPiece.info.angle;
                        followingPiece.logicalCorner = logicalCorner;
                        followingPiece = followingPiece.next;
                    } while(followingPiece.info.radius == piece.info.radius && sign(followingPiece.info.angle) == sign(piece.info.angle));
                }
            });

            for (var i = 0; i < gameInit.race.track.lanes.length; i++) {
                var lane = gameInit.race.track.lanes[i];
                lanes[lane.index] = new Lane();
                lanes[lane.index].info = lane;
            }

            // calculate best turbo place, i.e beginning of longest straight
            this.longestStraightStart().suitableForTurbo = true;

            // now, these guys are really something, starting in the middle of a corner
            if (pieces[0].logicalCorner) {
                this.physics.adjustCornerApproach(pieces[0].logicalCorner, this.racing());
            }
        };

        Engine.prototype.longestStraightStart = function () {
            var longestStraightStart;

            pieces.forEach(function (piece) {
                if (longestStraightStart == null || piece.consecutiveStraights() > longestStraightStart.consecutiveStraights())
                    longestStraightStart = piece;
            });

            return longestStraightStart;
        };

        Engine.prototype.informGameStart = function (gameStart) {
            this.gameStarts++;
            console.log('Race started');
            if (this.racing()) {
                console.log('NOW RACING !!!!!!!!!!!!!!!!!!!');
            }
        };

        Engine.prototype.racing = function () {
            return this.gameStarts > 1;
        };

        Engine.prototype.informTurboAvailable = function (turbo) {
            //            console.log('Turbo available');
            if (this.turboAvailable) {
                //                console.log("Already got one turbo available. Supposed to get two? Code some handling for this.");
                // there probably arent two turbos?
            } else {
                this.turboAvailable = turbo;
            }
        };

        Engine.prototype.informTurboStart = function (car) {
            if (this.ourCar(car)) {
                this.turboActive = this.turboRequestedWaitingForActivation;
                this.turboActivatedOnTick = this.gameTick;
                this.turboRequestedWaitingForActivation = null;
            }
        };

        Engine.prototype.informTurboEnd = function (car) {
            if (this.ourCar(car)) {
                this.turboActive = null;
                this.turboActivatedOnTick = null;
            }
        };

        Engine.prototype.ourCar = function (car) {
            return car.color == this.yourCar.color;
        };

        Engine.prototype.informCrash = function (car) {
            if (this.ourCar(car)) {
                //                console.log('');
                console.log('**** CRASH ****');
                console.log('Went off at angle: ' + this.state.car.angle);
                if (this.state.location.piece.logicalCorner) {
                    this.state.location.piece.logicalCorner.latestApproach().crash = true;
                }
            }
        };

        Engine.prototype.informLapFinished = function (lapFinished) {
            this.lapTimes.push(lapFinished.lapTime.millis);
            console.log("LAP TIMES: " + this.lapTimes.join(" -> "));
        };

        Engine.prototype.informGameEnd = function (gameEnd) {
            //            CSV.close();
            console.log('Race ended');
        };

        Engine.prototype.informTournamentEnd = function (tournamentEnd) {
            console.log('Watch replay at: https://helloworldopen.com/race-visualizer/?recording=https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/' + this.gameId + '.json');
        };

        Engine.prototype.getOurCar = function (carPositions) {
            for (var i = 0; i < carPositions.length; i++) {
                var carPosition = carPositions[i];
                if (carPosition.id.color == this.yourCar.color) {
                    return carPosition;
                }
            }
        };

        Engine.prototype.informCarPositions = function (carPositions) {
            var ourCar = this.getOurCar(carPositions);
            this.state.informCarPosition(this.gameTick, this.throttle, ourCar, this.turboActive != null);
            this.physics.inform(this.state);

            this.printHuman();
            this.printCSV();

            this.decideNextAction();
        };

        Engine.prototype.printHuman = function () {
            var precision = 1;
            var line = this.gameTick + '\t' + (this.state.car.turboActive ? "T " : "  ") + Util.roundPad(this.state.car.throttle, 1) + ' => ' + Util.roundPad(this.state.car.speed, 2) + ' (' + Util.signPrefixRoundPad(this.state.car.acceleration, 3) + ')' + '\t' + ' = ' + Util.whiteFrontPad(Util.round(this.state.car.angle, 1), 4) + ' = ' + '\t' + Util.whiteFrontPad(Util.roundPad(this.state.location.percentageLapDoneApprox(), 3), 4) + " (" + Util.whiteFrontPad(this.state.location.piece.index, 2) + ") " + (this.state.location.piece.isCorner() ? " C " : "   ") + (this.turboAvailable ? "(T:" + this.turboAvailable.turboDurationTicks + ":x" + this.turboAvailable.turboFactor + ") " : "") + (this.state.location.piece.isCorner() ? "(log. a: " + this.state.location.piece.logicalCorner.angle + " r: " + this.state.location.piece.logicalCorner.radius + ")" : "");

            lastLogLines.push(line);
            if (lastLogLines.length > 10) {
                lastLogLines.splice(0, 1);
            }
            console.log(line);
        };

        Engine.prototype.printCSV = function () {
            var csv = [];

            csv.push(this.gameTick);
            csv.push(Util.round(this.state.car.speed, 6));
            csv.push(Util.round(this.state.car.acceleration, 6));
            csv.push(this.state.car.turboActive);
            csv.push(Util.round(this.throttle, 6));
            csv.push(Util.round(this.state.car.angle, 6));
            csv.push(Util.round(this.state.car.angleSpeed, 6));
            csv.push(Util.round(this.state.car.angleAcceleration, 6));
            csv.push(Util.nvl(this.state.location.laneAdjustedRadius(), 0));
            //            CSV.write(csv.join('\t'));
        };

        Engine.prototype.decideNextAction = function () {
            this.nextAction = null;
            if (this.nextAction == null) {
                this.decideTurbo();
            }
            if (this.nextAction == null) {
                this.decideSwitchLane();
            }
            if (this.nextAction == null) {
                this.decideThrottle();
            }
        };

        Engine.prototype.decideTurbo = function () {
            if (!this.turboRequestedWaitingForActivation && !this.turboActive) {
                if (this.turboAvailable && this.state.location.piece.suitableForTurbo) {
                    this.nextAction = 0 /* TURBO */;
                    this.turboRequestedWaitingForActivation = this.turboAvailable;
                    this.turboAvailable = null;
                }
            }
        };

        Engine.prototype.decideSwitchLane = function () {
            // every 1000th tick at random
            if (this.racing() && Math.random() < 0.01) {
                if (this.state.location.lane.canSwitchLeft() && Math.random() < 0.5) {
                    this.nextAction = 1 /* SWITCH_LANE_LEFT */;
                } else if (this.state.location.lane.canSwitchRight() && Math.random() < 0.5) {
                    this.nextAction = 2 /* SWITCH_LANE_RIGHT */;
                }
            }
            //            this.nextAction = NextAction.SWITCH_LANE
            // now, make sure you are not doing anything like this when you already have requested turbo
        };

        Engine.prototype.decideThrottle = function () {
            this.nextAction = 3 /* THROTTLE */;
            this.throttle = this.getThrottle();
        };

        Engine.prototype.getThrottle = function () {
            // starting quali at middle of corner causes some trouble {
            if (this.gameTick == 0 && this.state.location.piece.logicalCorner) {
                this.physics.adjustCornerApproach(this.state.location.piece.logicalCorner, this.racing());
            }

            var onStraight = this.state.location.piece.isStraight();

            var distanceToNextLogicalCorner = this.state.location.distanceToNextLogicalCorner();

            // new corner
            if (this.state.location.nextLogicalCorner() != this.nextLogicalCorner) {
                this.nextLogicalCorner = this.state.location.nextLogicalCorner();
                this.physics.adjustCornerApproach(this.nextLogicalCorner, this.racing());
            }
            var nextCornerTargetSpeed = this.physics.estimateTargetSpeed(this.nextLogicalCorner, this.state.location.lane);
            var breakingDistanceToNextCorner = this.estimateBreakingDistanceToSpeed(nextCornerTargetSpeed);

            //            console.log('Distance to next logical corner: ' + distanceToNextLogicalCorner);
            //            console.log('Next logical corner target speed: ' + nextCornerTargetSpeed);
            //            console.log('Estimated breaking distance: ' + breakingDistanceToNextCorner);
            if (onStraight) {
                // in straight
                if (distanceToNextLogicalCorner > breakingDistanceToNextCorner) {
                    // - freeway
                    return 1;
                } else {
                    // - breaking zone
                    return this.adjustThrottleForTargetSpeed(nextCornerTargetSpeed);
                }
            } else {
                // in corner
                var currentCornerTargetSpeed = this.physics.estimateTargetSpeed(this.state.location.piece.logicalCorner, this.state.location.lane);

                //                console.log('Current logical corner target speed: ' + currentCornerTargetSpeed);
                if (distanceToNextLogicalCorner > breakingDistanceToNextCorner) {
                    // - cornering
                    return this.adjustThrottleForTargetSpeed(currentCornerTargetSpeed);
                } else {
                    // - breaking zone
                    return this.adjustThrottleForTargetSpeed(Math.min(currentCornerTargetSpeed, nextCornerTargetSpeed));
                }
            }
        };

        Engine.prototype.turboDurationLeft = function () {
            if (this.turboActive) {
                return this.turboActive.turboDurationTicks - (this.gameTick - this.turboActivatedOnTick);
            } else {
                return 0;
            }
        };

        Engine.prototype.estimateBreakingDistanceToSpeed = function (targetSpeed) {
            var simulatedCurrentSpeed = this.state.car.speed;
            var simulatedTurboDurationLeft = this.turboDurationLeft();
            var simulatedDistanceTravelled = 0;
            while (simulatedCurrentSpeed > targetSpeed) {
                simulatedDistanceTravelled += simulatedCurrentSpeed;
                simulatedCurrentSpeed += this.physics.estimateMaxDeceleration(simulatedCurrentSpeed, simulatedTurboDurationLeft > 0);
                simulatedTurboDurationLeft--;
            }
            return simulatedDistanceTravelled;
        };

        Engine.prototype.adjustThrottleForTargetSpeed = function (targetSpeed) {
            var currentSpeed = this.state.car.speed;
            var speedDifference = targetSpeed - currentSpeed;
            var turbo = this.state.car.turboActive;
            if (speedDifference < 0) {
                var maxDec = this.physics.estimateMaxDeceleration(currentSpeed, turbo);
                return Math.max(0, 1 - speedDifference / maxDec);
            } else {
                var maxAcc = this.physics.estimateMaxAcceleration(currentSpeed, turbo);
                return Math.min(1, speedDifference / maxAcc);
            }
        };
        return Engine;
    })();
    _Engine.Engine = Engine;
})(Engine || (Engine = {}));

module.exports = Engine;
