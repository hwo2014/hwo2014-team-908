var fs = require('fs');
var moment = require('../lib/moment');

var CSV;
(function (CSV) {
    var wstream;

    function open() {
        wstream = fs.createWriteStream('csv-data-' + moment().format('YYYYMMDD-HHmmss') + '.csv');
    }
    CSV.open = open;

    function write(line) {
        wstream.write(line + '\n');
    }
    CSV.write = write;

    function close() {
        wstream.end();
    }
    CSV.close = close;
})(CSV || (CSV = {}));

module.exports = CSV;
